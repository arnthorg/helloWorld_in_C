#include <stdio.h>
#define STACK_MAX_SIZE 25
struct Stack {
	char stack[STACK_MAX_SIZE];
	int size;
} stack;
void push_stack(char val) {
	if(stack.size < STACK_MAX_SIZE)
		stack.stack[stack.size++] = val;
}

char pop_stack() {
	char val;
	if(stack.size)
		val = stack.stack[--stack.size];
	else
		val = 0;
	return val;
}
char peek_stack() {
	return stack.stack[stack.size];
}
_Bool isEmpty_stack() {
	return stack.size == 0;
}

_Bool isFull_stack() {
	return stack.size == STACK_MAX_SIZE;
}

void init_stack(void) {
	stack.size=0;
}
//input: ((){}{()})
int main(int argc, char **argv)
{
	init_stack();
	_Bool nested = 1;
	char strr[STACK_MAX_SIZE];
	printf("Enter parenthesis: ");
	scanf("%s", strr);
	char *str;
	str = strr;
	while(*str != 0) {
		if(*str == '(' || *str == '{') {
			push_stack(*str);
		}
		else if(*str == ')' || *str == '}') {
			char val = pop_stack();
			if( *str == val+1 || *str == val+2)
				nested = 1;
			else
				nested = 0;
		}
	str++;
	}
	if(nested && isEmpty_stack())
		printf("nested properly\n");
	else
		printf("not nested properly\n");
	return 0;
}
