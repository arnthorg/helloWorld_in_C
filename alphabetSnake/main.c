#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define SIZE_MATRIX 10
#define microsecondsToMilliseconds 1000
#define timeToDelayMS 10
#define emptySquare '.'
#define startLetter 'A'
#define endLetter 'Z'
struct Position {
	short x;
	short y;
	char letter;
	char prevLetter;
};
void printMatrix(const char (*mat)[SIZE_MATRIX])
{
	for(int i = 0; i<SIZE_MATRIX; i++) {
		for(int j = 0; j<SIZE_MATRIX; j++) {
			printf("%c ", mat[i][j]);
		}
		printf("\n");
	}
}

void delayus(unsigned long int delayTime)
{
	struct timeval stop, start;
	mingw_gettimeofday(&start, NULL);
	mingw_gettimeofday(&stop, NULL);
	while((stop.tv_usec - start.tv_usec) <= delayTime )
		mingw_gettimeofday(&stop, NULL);
}

int main(int argc, char **argv)
{
	time_t t;
	char matrix[SIZE_MATRIX][SIZE_MATRIX];
	struct Position pos = {0,0,startLetter, endLetter};
	srand((unsigned) time(&t)); // seed for rand
	//init matrix
	for(int i = 0; i<SIZE_MATRIX; i++) {
		for(int j = 0; j<SIZE_MATRIX; j++) {
			matrix[i][j] = emptySquare;

		}
	}
	matrix[0][0] = pos.letter++;

	while(pos.letter <= endLetter) {
		// check if snake is trapped
		if( !(pos.x != 0 && matrix[pos.x - 1][pos.y] == emptySquare))
			if(!(pos.y != SIZE_MATRIX-1 && matrix[pos.x][pos.y+1] == emptySquare))
				if( !(pos.x != SIZE_MATRIX-1 && matrix[pos.x + 1][pos.y] == emptySquare))
					if( !(pos.y != 0 && matrix[pos.x][pos.y - 1] == emptySquare))
						break;

		switch(rand()%4) { //0 up, 1 right, 2 down, 3 left
		case 0: {
			if( pos.x != 0 && matrix[pos.x - 1][pos.y] == emptySquare) {
				pos.x--;
				matrix[pos.x][pos.y] = pos.letter++;
			}
		}
		break;
		case 1: {
			if( pos.y != SIZE_MATRIX-1 && matrix[pos.x][pos.y+1] == emptySquare) {
				pos.y++;
				matrix[pos.x][pos.y] = pos.letter++;
			}
		}
		break;
		case 2: {
			if( pos.x != SIZE_MATRIX-1 && matrix[pos.x + 1][pos.y] == emptySquare) {
				pos.x++;
				matrix[pos.x][pos.y] = pos.letter++;
			}
		}
		break;
		case 3: {
			if( pos.y != 0 && matrix[pos.x][pos.y - 1] == emptySquare) {
				pos.y--;
				matrix[pos.x][pos.y] = pos.letter++;
			}
		}
		break;
		}
		//print matrix and delay IF a valid move was made
		if(pos.letter != pos.prevLetter) {
			delayus(timeToDelayMS*microsecondsToMilliseconds);
			system("cls");
			printMatrix(matrix);
			pos.prevLetter = pos.letter;
		}
	}
	if(--pos.letter == endLetter)
		printf("completed successfully\n");
	else
		printf("snake got trapped\n");
	
}
