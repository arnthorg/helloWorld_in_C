#include <stdio.h>
#include <stdlib.h>
#include "tgmath.h"
#define type_name(T)	\
	_Generic((T),\
		char: "char",\
		int: "int", \
		unsigned int: "uint", \
		double :"double",\
		float: "float")
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 
	
int f(void) {
	static int staticint = 1337;
	return staticint++;
}
void swap(int *p, int *q) {
	int tmp = *p;
	*p = *q;
	*q = tmp;
}
int *testFun(void);
int main(int argc, char **argv)
{
	int p = 1, q = 2;
	printf("p: %i, q: %i\n", p, q);
	swap(&p, &q);
	printf("p: %i, q: %i\n", p, q);
	
	
	//int i = 10, j = 2, k=4, l = 1;
	/*
	printf("%i\n", i/j);
	printf("%i\n", (-i)/j);
	printf("%i\n", -(i/j));
	printf("\n");
	printf("%i\n", i%j);
	printf("%i\n", (-i)%j);
	printf("%i\n", (i)%(-j));
	printf("%i\n", -(i%j));
	
	printf("%i\n", i / j % k / l);
	printf("%i\n", (((i / j) % k) / l));
	*/
	//printf("%i\n", !!i);
	/*int i = 2, j = 1;
	char c = 'a';
	
	printf("%d", sizeof(i/j+c));*/
	/*
	char c = '\1';
	short s  = 2; 
unsigned int i = -3;
	long m = 5;
	float f = 6.5f;
	double d = 7.5;
	*/
	//printf("%s\n", type_name(c/i) );
	//printf("%s\n", type_name(d/s) );
	/*
	double c[] = {-1.0, -4.1, 1.0};
	double *p; 
	
	p = c;
	
	printf("%f \t %f\n", *p, c[0]);
	*/
	
	//printf("%i",(int []){1,2,3}[1]);
	
	//printf("%i\n", 1 ? 1:0);
	//printf("%i\n", 0 ? 1:0);
	
	/*for(int i= 0; i<5; i++) {
		printf("%i\n", f());
	
	}*/
	/* // testing out pointers, seeing if I can iterate through a number 
	 * // by using a char pointer. 
	int a = 0x1234;
	
	char *ptr = &a;
	
	printf("%x\n", (char)(*(ptr)));
	printf("%x\n", (char)(*(++ptr)));
	*/
	/*
	char str[] = "1234";
	
	printf("%i\n", atoi(str));
	*/
	/*
	unsigned char c = 15; 
	printf("15>>1 " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(c>>1));
	printf("\n");
	
	printf("15<<1 " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(c<<1));
	printf("\n");
	printf("%i\n", 2<<3);
	
	printf("-8>>1 " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(-8>>1));
	printf("\n-8<<1 " BYTE_TO_BINARY_PATTERN, BYTE_TO_BINARY(-8<<1));
	
	printf("\n%i\n", -8>>1);
	*/
	
	//printf("%i\n", *testFun());
	/*
	int *ptr, *qtr;
	int i = 2;
	ptr = &i;
	qtr = &i;
	
	qtr = &ptr;
	printf("%p\n", qtr);
	printf("%p\n", &ptr);
	
	printf("%p\n", ptr);
	printf("%p\n", &ptr);
	printf("%i\n", *&i);
	*/
	return 0;
}

int *testFun(void) {
	static int i = 10;
	return &i;
}