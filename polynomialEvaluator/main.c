#include <stdio.h>

double power(double value, int power) {
	double product = value;
	if(power == 0) {
		return 1;
	}
	else {
		for(int i=1; i<power;i++) {
			product *= value;
		}
		
	}
	return product;
}
int main(int argc, char **argv)
{
	float x = 2.55;
	printf("3x3 - 5x2 + 6|x=2.55 = %e", 3*power(x, 3) - 5*power(x, 2) + 6*power(x, 0));
	return 0;
}
