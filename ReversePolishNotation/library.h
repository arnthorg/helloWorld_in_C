#include <math.h>

#define STACK_MAX_SIZE 25
struct Stack {
	int stack[STACK_MAX_SIZE];
	int size;
} stack;
void push_stack(int val) {
	if(stack.size < STACK_MAX_SIZE)
		stack.stack[stack.size++] = val;
}

int pop_stack() {
	int val;
	if(stack.size)
		val = stack.stack[--stack.size];
	else
		val = 0;
	return val;
}
int peek_stack() {
	return stack.stack[stack.size];
}
_Bool isEmpty_stack() {
	return stack.size == 0;
}

_Bool isFull_stack() {
	return stack.size == STACK_MAX_SIZE;
}

void init_stack(void) {
	stack.size=0;
}


int strToInt(char *str){
	char* ptr = str; 
	int length = 0;
	int number = 0;
	for(;*ptr;ptr++,length++);
	for(int i=0;i<length;i++) {
		number += (str[i]-'0')*pow(10,(length-i-1));
	}
	return number;
}