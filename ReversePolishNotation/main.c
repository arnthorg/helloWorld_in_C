#include <stdio.h>
#include "library.h"

int main(int argc, char **argv)
{
	init_stack();
	char statement[STACK_MAX_SIZE];
	printf("Enter operands and operators followed by =: ");
	scanf("%[^\n]", statement);

	
	char *ptr;
	ptr = statement;
	while(*ptr) {

		int tmp = 0;
		if(*ptr>='0' && *ptr<='9') {
			tmp = *ptr-'0';
			while(*(ptr+1) != ' ') { 	//tests if next number isn't space
				ptr++;					// and adds it to tmp;
				tmp *= 10;				// could come in handy for a strToInt func
				tmp += *ptr-'0';
			}
			push_stack(tmp);
		}
		else if(*ptr == '+') {
			push_stack(pop_stack() + pop_stack());
		}
		else if(*ptr == '-') {
			push_stack(pop_stack() - pop_stack());
		}
		else if(*ptr == '*') {
			push_stack(pop_stack() * pop_stack());
		}
		else if(*ptr == '/') {
			push_stack(pop_stack() / pop_stack());
		}
	ptr++;
	}
	printf("Value is: %i\n", pop_stack());
}
