#include <stdio.h>
int main(int argc, char **argv)
{
	int hour, minute;
	char aOrP;
	printf("Enter time[hh:mm]: ");
	scanf("%2d:%2i", &hour, &minute); // %2d for reading the leading zero when time is <=12

	if(hour>12) {
		hour %= 12;
		aOrP = 'P';
	}
	else {
		aOrP = 'A';
	}
	printf("in twelve h: %2.2i:%2.2i %c%c\n", hour, minute, aOrP, 'M'); 
	//%2.2i to always print leading zero and take 2 spaces. 
	return 0;
}
