#include <stdio.h>
#include <time.h>

void delayus(unsigned long int delayTime) {
	struct timeval stop, start;
	mingw_gettimeofday(&start, NULL);
	mingw_gettimeofday(&stop, NULL);
	while((stop.tv_usec - start.tv_usec) < delayTime )
		mingw_gettimeofday(&stop, NULL);
}

int main(int argc, char **argv)
{
	struct timeval stop, start;
	for(int i=0; i<10; i++) {
		mingw_gettimeofday(&start, NULL);
		delayus(500*1000);
		mingw_gettimeofday(&stop, NULL);
		printf("took %lu microsec\n", stop.tv_usec - start.tv_usec);
	}
	return 0;
}
