//prints out squares of numbers
#define squared 253
#include <stdio.h>

int main(int argc, char **argv)
{
	int n;
	printf("squares up to?: \n");
	scanf("%i", &n);
	printf("n\tn%c\n", squared);
	for(int i=1; i<=n; i++) {
		printf("%-2i\t%-3i\n", i, i*i);
	}
	return 0;
}
