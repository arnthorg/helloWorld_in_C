#include <stdio.h>
#include <inttypes.h>

int main(int argc, char **argv)
{
	int hexa = 0xdeadbeef;
	double number = 12e9;
	char c = 'c';
	_Bool boolean = 1;
	long int longValue = 60*60*24*12*365uL;
	long long int longLongValue = 60*60*24*12*365*3;
	int hexad = 0xabcdu;
	printf("%x\n", hexa);
	printf("%X\n", hexa);
	printf("%#x\n", hexa);
	printf("%#X\n", hexa);
	printf("%e\n", number);
	printf("%c\n", c);
	printf("%i\n", boolean);
	printf("%li\n", longValue);
	printf("%I64u\n", longLongValue);
	printf("%X", hexad);
	return 0;
}
